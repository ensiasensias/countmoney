package com.nespresso.sofa.recruitment.count.the.money;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class CountTheMoneyTest {

    @Test
    public void shouldRefundIfNotYetPaid() {
        CountTheMoney countTheMoney = new CountTheMoney();
        Participant jon = countTheMoney.addParticipant();
        countTheMoney.withBill("hotel", 150.45).withBill("food", 45.20);

        assertEquals(195.65, jon.owes(), 0.);
    }

    @Test
    public void shouldRefundNoOneIfEverythingIsPaid() {
        CountTheMoney countTheMoney = new CountTheMoney();
        Participant jon = countTheMoney.addParticipant();
        countTheMoney.withBill("hotel", 150.45).withBill("food", 45.20);
        jon.pays(195.65);

        assertEquals(0., jon.owes(), 0.);
    }

    @Test
    public void shouldRefundTheOtherOne() {
        CountTheMoney countTheMoney = new CountTheMoney();
        Participant jon = countTheMoney.addParticipant();
        countTheMoney.withBill("hotel", 150.45).withBill("food", 44.05);
        jon.pays(44.05);

        Participant greg = countTheMoney.addParticipant();
        greg.pays(150.45);

        assertEquals(53.2, jon.owes(), 0.);
        assertEquals(0., greg.owes(), 0.);

        assertEquals(53.2, jon.owes(greg), 0.);
    }

    @Test
    public void shouldRefundWhoPaidForOthers() {
        CountTheMoney countTheMoney = new CountTheMoney();
        Participant jon = countTheMoney.addParticipant();
        countTheMoney.withBill("hotel", 50).withBill("food", 40);
        jon.pays(40);

        Participant greg = countTheMoney.addParticipant();
        greg.pays(50);

        Participant olivier = countTheMoney.addParticipant();

        assertEquals(0., jon.owes(), 0.);
        assertEquals(0., greg.owes(), 0.);
        assertEquals(30., olivier.owes(), 0.);

        assertEquals(10., olivier.owes(jon), 0.);
        assertEquals(20., olivier.owes(greg), 0.);
    }

    @Test
    public void shouldSupportGroups() {
        CountTheMoney countTheMoney = new CountTheMoney();
        Participant jon = countTheMoney.addParticipant();
        Participant greg = countTheMoney.addParticipant();

        countTheMoney.withBill("hotel", 50).withBill("food", 40).withBill("gas", 20, jon, greg);

        jon.pays(40);
        greg.pays(50);
        greg.pays(20);

        Participant olivier = countTheMoney.addParticipant();

        assertEquals(0., jon.owes(), 0.);
        assertEquals(0., greg.owes(), 0.);
        assertEquals(30., olivier.owes(), 0.);

        assertEquals(30., olivier.owes(greg), 0.);
    }

    @Test
    public void shouldAllowContributors() {
        CountTheMoney countTheMoney = new CountTheMoney();
        Participant jon = countTheMoney.addParticipant();
        Participant greg = countTheMoney.addParticipant();
        Participant olivier = countTheMoney.addParticipant();

        countTheMoney.withBill("hotel", 50).withBill("food", 40).withBill("gas", 20, jon, greg).withBill("activities", 210);
        jon.contribute(210.0);
        greg.pays(40).pays(50).pays(20);

        assertEquals(0., jon.owes(), 0.);
        assertEquals(100., olivier.owes(), 0.);

        assertEquals(100., olivier.owes(jon), 0.);
    }

    @Test
    public void shouldAcceptOneBanker() {
        CountTheMoney countTheMoney = new CountTheMoney();
        Participant jon = countTheMoney.addParticipant();
        Participant greg = countTheMoney.addParticipant();
        Participant olivier = countTheMoney.addParticipant();
        Participant robain = countTheMoney.addParticipant();

        countTheMoney.withBill("hotel", 50).withBill("food", 40).withBill("gas", 20, jon, greg).withBill("activities", 210).withBanker(jon);
        greg.pays(40).pays(50).pays(20);
        olivier.pays(210);

        assertEquals(75., robain.owes(), 0.);
        assertEquals(160., jon.owes(), 0.);

        assertEquals(75., robain.owes(jon), 0.);
        assertEquals(135., jon.owes(olivier), 0.);
        assertEquals(25., jon.owes(greg), 0.);
    }

}
